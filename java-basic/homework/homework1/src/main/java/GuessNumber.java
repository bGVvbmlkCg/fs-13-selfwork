import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {
    static int MAX_RANDOM_NUMBER = 100;
    static String WELCOME_TEXT = "Let the game begin! To exit type: -1";
    static String NUMBER_TOO_SMALL = "Your number is too small. Please, try again.";
    static String NUMBER_TOO_BIG = "Your number is too big. Please, try again.";
    static String CONGRATULATIONS = "Congratulations, %s!";
    static String GUESSED_NUMBERS = "Guessed numbers: ";
    static String INVALID_VALUE = "%s is not valid number!";
    static boolean DEBUG = true;

    static String user_name;
    static int[] guessed_numbers = new int[0];
    static int guessNumber;
    static int realNumber;

    static Scanner scanner = new Scanner(System.in);
    static Random randomizer = new Random();

    public static void printNumberTooSmall() {
        print(NUMBER_TOO_SMALL);
    }

    public static void printNumberTooBig() {
        print(NUMBER_TOO_BIG);
    }

    public static void printCongratulations() {
        print(String.format(CONGRATULATIONS, user_name));
    }

    public static void printInvalidValue(String value) {
        print(String.format(INVALID_VALUE, value));
    }

    public static int getGuessNumber() {
        print("Enter a number: ");
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        } else {
            String value = scanner.next();
            printInvalidValue(value);
            return getGuessNumber();
        }
    }

    static void print(String text) {
        System.out.println(text);
    }

    public static void printNumberIfDebug(int number) {
        if (DEBUG) {
            print(String.format("Number: %d", number));
        }
    }

    public static void printWelcomeText() {
        print(WELCOME_TEXT);
    }

    public static int getRandomNumber() {
        return randomizer.nextInt(MAX_RANDOM_NUMBER + 1);
    }

    public static String getUserName() {
        print("Enter your name: ");
        return scanner.nextLine();
    }

    static void updateRealNumber() {
        realNumber = getRandomNumber();
        printNumberIfDebug(realNumber);
    }

    static void addGuessedNumber(int guessedNumber) {
        int newArrayLength = guessed_numbers.length;
        guessed_numbers = Arrays.copyOf(guessed_numbers, newArrayLength + 1);
        guessed_numbers[newArrayLength] = guessedNumber;
    }

    static void printGuessedNumbers() {
        String message = String.format("%s%s", GUESSED_NUMBERS, Arrays.toString(guessed_numbers));
        print(message);
    }

    public static void runGame() {
        updateRealNumber();

        while (true) {
            guessNumber = getGuessNumber();

            if (guessNumber == -1) {
                break;
            } else if (guessNumber < realNumber) {
                printNumberTooSmall();
            } else if (guessNumber > realNumber) {
                printNumberTooBig();
            } else {
                printCongratulations();
                addGuessedNumber(realNumber);
                updateRealNumber();
            }
        }
    }

    static void onGameStart() {
        printWelcomeText();
    }

    static void onGameEnd() {
        printGuessedNumbers();
    }

    static void updateUserName() {
        user_name = getUserName();
    }

    public static void main(String[] args) {
        updateUserName();

        onGameStart();
        runGame();
        onGameEnd();
    }
}
