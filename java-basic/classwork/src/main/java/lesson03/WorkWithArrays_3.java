package lesson03;

import java.util.Random;
import java.util.StringJoiner;
import java.util.function.Function;

public class WorkWithArrays_3 {

    public static void printElementsWithProcessor(int[] row, Function<Integer, String> processor) {
        StringJoiner sb = new StringJoiner(", ");
        for (int el : row) sb.add(processor.apply(el));
        System.out.println(sb.toString());
    }

    public static void main(String[] args) {
        Random random = new Random();
        int N = 5;
        int[][] a = new int[N][];

        for (int i = 0; i < a.length; i++) {
            a[i] = new int[N];
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = random.nextInt(20);
            }
        }

        for (int[] row : a) {
            printElementsWithProcessor(row, x -> x % 2 == 0 ? String.format("%4s", x) : "----");
        }
    }
}
