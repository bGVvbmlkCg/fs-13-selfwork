package lesson03;

public class integerToString {
    public static String intToString(int number) {
        StringBuilder sb = new StringBuilder();
        boolean isNegative = false;

        if (number < 0) {
            isNegative = true;
            number = Math.abs(number);
        }

        do {
            sb.append((char) (number % 10 + '0'));
            number = number / 10;
        } while (number != 0);

        if (isNegative) {
            sb.append("-");
        }

        return sb.reverse().toString();
    }

    public static int stringToInt(String string) {
        int number = 0;
        for (int i = 0; i < string.length(); i++) {
            number += (string.charAt(i) - 48) * Math.pow(10, (string.length() - i - 1));
        }
        return number;
    }

    public static void main(String[] args) {
        int number = -123;
        String s = intToString(number);
        Integer i = stringToInt(s);

        System.out.printf("String: %s\n", s);
        System.out.printf("Number: %d\n", i);

    }
}
