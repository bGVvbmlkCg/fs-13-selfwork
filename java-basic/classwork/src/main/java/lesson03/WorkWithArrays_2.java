package lesson03;

import java.util.Arrays;
import java.util.Random;

public class WorkWithArrays_2 {
    static int ARRAY_LENGTH = 10;
    static int MAX_ITEM_VALUE = 10;
    static Random random = new Random();
    static int[] ints = new int[ARRAY_LENGTH];

    static int[] even = new int[0];
    static int[] odd = new int[0];

    public static void main(String[] args) {

        for (int idx = 0; idx < ints.length; idx++) {
            ints[idx] = random.nextInt(MAX_ITEM_VALUE);
        }

        for (int number : ints) {
            if (number % 2 == 0) {
                even = Arrays.copyOf(even, even.length + 1);
                even[even.length - 1] = number;
            } else {
                odd = Arrays.copyOf(odd, odd.length + 1);
                odd[odd.length - 1] = number;
            }
        }

        System.out.format("Even: %s\nOdd: %s\n", Arrays.toString(even), Arrays.toString(odd));
    }
}
